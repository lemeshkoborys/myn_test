### signup endpoint
`/api/v1/sign-up/`
##### method - POST
##### request body example:

```json
{
  "email": "email@email.com",
  "password": "password",
  "first_name": "first_name",
  "last_name": "last_name",
  "mailchimp_api_key": "API_KEY",
  "mailchimp_username": "username"
}
```

### obtain and refresh JWT endpoints
`/api/v1/token/obtain/`
`/api/v1/token/refresh/`
#####method - POST
##### request body params exmaple:
```json
{
  "email": "email@email.com",
  "password": "password"
}
```

### retrieve current user endpoint
`/api/v1/accounts/current_user/`

### update current user data endpoint
`/api/v1/accounts/update/`
#####method - PUT
#####request body parmas example:

```json
{
    "id": 1,
    "email": "admin@gmail.com",
    "first_name": "",
    "last_name": "",
    "last_login": "2019-11-12T10:08:47Z",
    "is_active": true
}
```
also mailchimp api key and username could be setted up here

### get reports endpoint
`/api/v1/progress-chart/{campaign_id}/`

### get open details report endpoint
`/api/v1/progress-chart/opens-details/`

### get click details report endpoint
`/api/v1/progress-chart/click-details/`

### get sent to report endpoint
`/api/v1/progress-chart/sent-to/`

### get abuse reports endpoint
`/api/v1/progress-chart/abuse-reports/`

### get abuse reports endpoint
`/api/v1/progress-chart/abuse-reports/`

### get first 24-h statistics endpoint
`/api/v1/performance-chart/abuse-reports/`

### get click/open data with timestamps endpoint (email-activity)
`/api/v1/performance-chart/email-activity/`

