CAMPAIGNS_URL = 'https://{}.api.mailchimp.com/3.0/campaigns'

EMAIL_ACTIVITY_URL = 'https://{}.api.mailchimp.com/3.0/reports/{}/email-activity'
