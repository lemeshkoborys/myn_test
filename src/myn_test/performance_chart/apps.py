from django.apps import AppConfig


class PerformanceChartConfig(AppConfig):
    name = 'performance_chart'
