from django.contrib import admin
from . import models


@admin.register(models.CampaignReportTimeseries)
class CampaignReportTimeseriesModelAdmin(admin.ModelAdmin):
    list_display = (
        'timestamp',
        'campaign_id',
        'emails_sent',
        'unique_opens',
        'recipients_clicks',
    )


@admin.register(models.EmailActivity)
class EmailActivityModelAdmin(admin.ModelAdmin):
    list_display = (
        'action',
        'email_address',
        'timestamp',
    )
    list_filter = (
        'action',
    )
