from django.urls import path
from . import views


urlpatterns = [
    path(
        'timeseries/',
        views.TimeseriesViewSet.as_view({
            'get': 'list'
        }),name='performance_report'),
    path('email-activity/',
         views.EmailActivityViewSet.as_view({'get': 'retrieve'}),
         name='email_activity'),
]
