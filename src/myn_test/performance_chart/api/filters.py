from django_filters import rest_framework as filters
from myn_test.performance_chart import models as performance_models


class TimeseriesFilter(filters.FilterSet):
    timestamp__gt = filters.DateTimeFilter(field_name='timestamp',
                                           lookup_expr='gt')
    timestamp__lt = filters.DateTimeFilter(field_name='timestamp',
                                           lookup_expr='lt')

    class Meta:
        model = performance_models.CampaignReportTimeseries
        fields = ('timestamp__gt', 'timestamp__lt')

