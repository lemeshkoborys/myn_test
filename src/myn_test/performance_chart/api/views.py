from rest_framework.viewsets import GenericViewSet
from rest_framework.mixins import ListModelMixin, RetrieveModelMixin
from rest_framework.decorators import api_view
from rest_framework.response import Response
from myn_test.performance_chart import models as performance_models
from django_filters import rest_framework as filters
from .filters import TimeseriesFilter
from . import serializers


class TimeseriesViewSet(ListModelMixin, GenericViewSet):
    serializer_class = serializers.PerformanceChatReportTimeseriesModelSerializer
    queryset = performance_models.CampaignReportTimeseries.objects.all()
    filter_backends = (filters.backends.DjangoFilterBackend, )
    filterset_class = TimeseriesFilter


class EmailActivityViewSet(RetrieveModelMixin, GenericViewSet):

    serializer_class = serializers.EmailActivitySerializer


    def get_object(self):
        email_activity = performance_models.EmailActivity.objects.all()
        data = {
            'clicks': email_activity.filter(action='click'),
            'opens': email_activity.filter(action='open'),
            'total_clicks': email_activity.filter(action='click').count(),
            'total_opens': email_activity.filter(action='open').count()
        }
        return data
