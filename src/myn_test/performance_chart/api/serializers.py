from django.utils import dateparse
from rest_framework import serializers
from myn_test.performance_chart import models


class PerformanceChatReportTimeseriesModelSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.CampaignReportTimeseries
        fields = (
            'timestamp',
            'emails_sent',
            'unique_opens',
            'recipients_clicks',
        )


class ClickSerializer(serializers.Serializer):
    email_address = serializers.EmailField()
    timestamp = serializers.DateTimeField()


class OpenSerializer(serializers.Serializer):
    email_address = serializers.EmailField()
    timestamp = serializers.DateTimeField()


class EmailActivitySerializer(serializers.Serializer):

    clicks = ClickSerializer(many=True)
    opens = OpenSerializer(many=True)
    total_clicks = serializers.IntegerField()
    total_opens = serializers.IntegerField()
