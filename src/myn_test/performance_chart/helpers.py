import requests
from .constants import *


def get_email_activities(server, campaign_id, user):
    email_activities = requests.get(
        url=EMAIL_ACTIVITY_URL.format(server, campaign_id),
        auth=(user.mailchimp_username, user.mailchimp_api_key)
    ).json()
    data = []
    for email in email_activities['emails']:
        data = [x for x in email['activity']]
        for action in data:
            action['email'] = email['email_address']
    return data

