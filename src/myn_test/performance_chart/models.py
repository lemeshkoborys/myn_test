from django.db import models
from django.contrib.postgres.fields import JSONField


class CampaignReportTimeseries(models.Model):  # first 24h campaign activity

    campaign_id = models.CharField(max_length=120)
    timestamp = models.DateTimeField(unique=True)
    emails_sent = models.PositiveIntegerField()
    unique_opens = models.PositiveIntegerField()
    recipients_clicks = models.PositiveIntegerField()


class EmailActivity(models.Model):  # not unique opens and clicks
    action = models.CharField(max_length=120)
    email_address = models.EmailField()
    timestamp = models.DateTimeField()

    @property
    def total_clicks(self):
        return self.objects.filter(action='click').count()
