from celery import Celery
from myn_test.accounts.models import User
from .models import EmailActivity
from myn_test.progress_chart import helpers as progress_helpers
from .helpers import *


app = Celery()
users = User.objects.exclude(mailchimp_api_key__isnull=True)


@app.task
def retrieve_email_activity():
    for user in users:
        server = user.mailchimp_api_key[-3:]
        campaigns_ids = [x.get('id')
                         for x in progress_helpers.get_campaigns(user, server)]
        email_activities = [get_email_activities(server, campaign_id, user)
                            for campaign_id in campaigns_ids]
        for actions in email_activities:
            [EmailActivity.objects.update_or_create(
                email_address=action['email'],
                action=action['action'],
                timestamp=action['timestamp']
            ) for action in actions]


