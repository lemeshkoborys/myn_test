import requests
from celery import Celery
from celery.schedules import crontab
from myn_test.accounts.models import User
from myn_test.performance_chart.models import CampaignReportTimeseries
from . import models
from .constants import *
from .helpers import *


app = Celery()
users = User.objects.exclude(mailchimp_api_key__isnull=True)


@app.task
def retrieve_campaigns():
    for user in users:
        server = user.mailchimp_api_key[-3:]
        campaigns_ids = [x.get('id') for x in get_campaigns(user, server)]
        reports = get_reports(user, server, campaigns_ids)
        for report in reports:
            models.CampaignReport.objects.update_or_create(
                campaign_id=report['campaign_id'],
                campaign_title=report['campaign_title'],
                emails_sent=report['emails_sent'],
                abuse_reports=report['abuse_reports'],
                unsubscribed=report['unsubscribed'],
                send_time=report['send_time'],
                bounces=report['bounces'],
                forwards=report['forwards'],
                opens=report['opens'],
                clicks=report['clicks'],
                delivery_status=report['delivery_status']
            )
            update_or_create_timeseries(report['timeseries'],
                                        report['campaign_id'])


@app.task
def retrieve_open_details():
    for user in users:
        server = user.mailchimp_api_key[-3:]
        campaigns_ids = [x.get('id') for x in get_campaigns(user, server)]
        opens_details = [get_opens_details(user, server, campaign_id)
                         for campaign_id in campaigns_ids]
        for od in opens_details:
            models.OpensDetails.objects.update_or_create(
                members=od['members'],
                campaign_id=od['campaign_id']
            )


@app.task
def retrieve_sent_to():
    for user in users:
        server = user.mailchimp_api_key[-3:]
        campaigns_ids = [x.get('id') for x in get_campaigns(user, server)]
        sent_to = [get_sent_to(user, server, campaign_id)
                   for campaign_id in campaigns_ids]
        for st in sent_to:
            models.SentTo.objects.update_or_create(
                sent_to=st['sent_to'],
                campaign_id=st['campaign_id']
            )


@app.task
def retrieve_abuse_reports():
    for user in users:
        server = user.mailchimp_api_key[-3:]
        campaigns_ids = [x.get('id') for x in get_campaigns(user, server)]
        abuse_reports = [get_abuse_reports(user, server, campaign_id)
                         for campaign_id in campaigns_ids]
        for ar in abuse_reports:
            models.AbuseReports.objects.update_or_create(
                campaign_id=ar['campaign_id'],
                abuse_reports=ar['abuse_reports']
            )


@app.task
def retrieve_click_details():
    for user in users:
        server = user.mailchimp_api_key[-3:]
        campaigns_ids = [x.get('id') for x in get_campaigns(user, server)]
        click_details = [get_click_details(user, server, campaign_id)
                         for campaign_id in campaigns_ids]
        for cd in click_details:
            models.ClickDetails.objects.update_or_create(
                campaign_id=cd['campaign_id'],
                urls_clicked=cd['urls_clicked']
            )
