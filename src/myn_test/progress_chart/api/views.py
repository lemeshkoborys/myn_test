from rest_framework.mixins import RetrieveModelMixin
from rest_framework.viewsets import GenericViewSet
from myn_test.progress_chart import models
from . import serializers


class ProgressChartViewSet(RetrieveModelMixin, GenericViewSet):
    serializer_class = serializers.ProgressChartSerializer
    queryset = models.CampaignReport.objects.all()


class ProgressChartOpensDetailsViewSet(RetrieveModelMixin, GenericViewSet):
    serializer_class = serializers.ProgressChartOpensDetailsSerializer
    queryset = models.OpensDetails.objects.all()


class ProgressChartSentToViewSet(RetrieveModelMixin, GenericViewSet):
    serializer_class = serializers.ProgressChartSentToModelSerializer
    queryset = models.SentTo.objects.all()


class ProgressChartAbuseReportsViewSet(RetrieveModelMixin, GenericViewSet):
    serializer_class = serializers.ProgressChartAbuseReportsModelSerializer
    queryset = models.AbuseReports.objects.all()


class ProgressChartClickDetailsViewSet(RetrieveModelMixin, GenericViewSet):
    serializer_class = serializers.ProgressChartClickDetailsModelSerializer
    queryset = models.ClickDetails.objects.all()
