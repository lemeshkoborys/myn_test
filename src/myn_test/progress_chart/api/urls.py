from django.urls import path
from . import views

urlpatterns = [
    path(
        '<str:pk>/',
        views.ProgressChartViewSet.as_view({'get': 'retrieve'}),
        name='progress_chart'),
    path(
        '<str:pk>/opens-details/',
        views.ProgressChartOpensDetailsViewSet.as_view({'get': 'retrieve'}),
        name='opens_details'
    ),
    path(
        '<str:pk>/sent-to/',
        views.ProgressChartSentToViewSet.as_view({'get': 'retrieve'}),
        name='sent_to'
    ),
    path(
        '<str:pk>/abuse-reports/',
        views.ProgressChartAbuseReportsViewSet.as_view({'get': 'retrieve'}),
        name='abuse_reports'
    ),
    path(
        '<str:pk>/click-details/',
        views.ProgressChartClickDetailsViewSet.as_view({'get': 'retrieve'}),
        name='click_details'
    ),
]
