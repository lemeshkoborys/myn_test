from rest_framework.serializers import ModelSerializer
from myn_test.progress_chart import models


class ProgressChartSerializer(ModelSerializer):

    class Meta:
        model = models.CampaignReport
        fields = (
            'campaign_id',
            'campaign_title',
            'emails_sent',
            'abuse_reports',
            'unsubscribed',
            'send_time',
            'unopened',
            'total_bounces',
            'total_delivered',
            'forwards',
            'clicks',
            'bounces',
            'delivery_status',
        )


class ProgressChartOpensDetailsSerializer(ModelSerializer):

    class Meta:
        model = models.OpensDetails
        fields = '__all__'


class ProgressChartSentToModelSerializer(ModelSerializer):

    class Meta:
        model = models.SentTo
        fields = '__all__'


class ProgressChartAbuseReportsModelSerializer(ModelSerializer):

    class Meta:
        model = models.AbuseReports
        fields = '__all__'


class ProgressChartClickDetailsModelSerializer(ModelSerializer):

    class Meta:
        model = models.ClickDetails
        fields = '__all__'
