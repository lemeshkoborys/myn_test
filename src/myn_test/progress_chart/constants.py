REPORTS_URL = 'https://{}.api.mailchimp.com/3.0/reports/{}/'

CAMPAIGNS_URL = 'https://{}.api.mailchimp.com/3.0/campaigns'

OPENS_DETAILS_URL = 'https://{}.api.mailchimp.com/3.0/reports/{}/open-details'

SENT_TO_URLS = 'https://{}.api.mailchimp.com/3.0/reports/{}/sent-to'

ABUSE_REPORTS_URL = 'https://{}.api.mailchimp.com/3.0/reports/{}/abuse-reports'

CLICK_DETAILS_URL = 'https://{}.api.mailchimp.com/3.0/reports/{}/click-details'
