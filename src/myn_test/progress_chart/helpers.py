import requests
from django.db import transaction
from .constants import *
from myn_test.performance_chart.models import CampaignReportTimeseries


def get_reports(user, server, campaigns_ids):
    reports = [requests.get(
        REPORTS_URL.format(server, campaign_id),
        auth=(user.mailchimp_username, user.mailchimp_api_key)).json()
               for campaign_id in campaigns_ids]
    for r, campaign_id in zip(reports, campaigns_ids):
        r['campaign_id'] = campaign_id
    return reports


def get_campaigns(user, server):
    campaigns = requests.get(
        CAMPAIGNS_URL.format(server),
        auth=(
            user.mailchimp_username,
            user.mailchimp_api_key
        )).json()['campaigns']
    return campaigns


def get_opens_details(user, server, campaign_id):
    opens_details = requests.get(
        OPENS_DETAILS_URL.format(server, campaign_id),
        auth=(user.mailchimp_username, user.mailchimp_api_key)
    ).json()
    opens_details['campaign_id'] = campaign_id
    return opens_details


def get_sent_to(user, server, campaign_id):
    sent_to = requests.get(
        SENT_TO_URLS.format(server, campaign_id),
        auth=(user.mailchimp_username, user.mailchimp_api_key)
    ).json()
    sent_to['campaign_id'] = campaign_id
    return sent_to


def get_abuse_reports(user, server, campaign_id):
    abuse_reports = requests.get(
        ABUSE_REPORTS_URL.format(server, campaign_id),
        auth=(user.mailchimp_username, user.mailchimp_api_key)
    ).json()
    abuse_reports['campaign_id'] = campaign_id
    return abuse_reports


def get_click_details(user, server, campaign_id):
    click_details = requests.get(
        CLICK_DETAILS_URL.format(server, campaign_id),
        auth=(user.mailchimp_username, user.mailchimp_api_key)
    ).json()
    click_details['campaign_id'] = campaign_id
    return click_details


@transaction.atomic
def update_or_create_timeseries(timeseries, campaign_id):
    for t in timeseries:
        CampaignReportTimeseries.objects.update_or_create(
            campaign_id=campaign_id,
            timestamp=t['timestamp'],
            emails_sent=t['emails_sent'],
            unique_opens=t['unique_opens'],
            recipients_clicks=t['recipients_clicks']
        )
