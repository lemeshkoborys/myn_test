from django.apps import AppConfig


class CampaignsConfig(AppConfig):
    name = 'myn_test.progress_chart'
