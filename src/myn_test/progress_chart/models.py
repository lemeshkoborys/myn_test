from django.db import models
from django.contrib.postgres.fields import JSONField
from django.core.validators import MinValueValidator


class OpensDetails(models.Model):
    members = JSONField()
    campaign_id = models.CharField(max_length=120, primary_key=True)


class SentTo(models.Model):
    sent_to = JSONField()
    campaign_id = models.CharField(max_length=120, primary_key=True)


class AbuseReports(models.Model):
    abuse_reports = JSONField()
    campaign_id = models.CharField(max_length=120, primary_key=True)


class ClickDetails(models.Model):
    urls_clicked = JSONField()
    campaign_id = models.CharField(max_length=120, primary_key=True)


class CampaignReport(models.Model):

    campaign_id = models.CharField(max_length=120, primary_key=True)
    campaign_title = models.CharField(max_length=256)
    emails_sent = models.PositiveIntegerField()
    abuse_reports = models.PositiveIntegerField()
    unsubscribed = models.PositiveIntegerField()
    send_time = models.DateTimeField()
    bounces = JSONField()
    forwards = JSONField()
    opens = JSONField()
    clicks = JSONField()
    delivery_status = JSONField()

    @property
    def total_bounces(self):
        return self.bounces['hard_bounces'] + self.bounces['soft_bounces']

    @property
    def unopened(self):
        return self.emails_sent - self.opens['opens_total']

    @property
    def total_delivered(self):
        if self.delivery_status['enabled']:
            return self.delivery_status['emails_sent']
