from django.contrib import admin
from . import models


@admin.register(models.CampaignReport)
class CampaignReportModelAdmin(admin.ModelAdmin):

    list_display = (
        'campaign_title',
        'emails_sent',
        'abuse_reports',
        'clicks',
        'opens',
        'unsubscribed',
        'bounces',
    )


@admin.register(models.OpensDetails)
class OpensDetailsModelAdmin(admin.ModelAdmin):
    list_display = (
        'campaign_id',
        'members',
    )


@admin.register(models.SentTo)
class SentToModelAdmin(admin.ModelAdmin):
    list_display = (
        'campaign_id',
        'sent_to',
    )


@admin.register(models.AbuseReports)
class AbuseReportsModelAdmin(admin.ModelAdmin):
    list_display = (
        'campaign_id',
        'abuse_reports',
    )


@admin.register(models.ClickDetails)
class AbuseReportsModelAdmin(admin.ModelAdmin):
    list_display = (
        'campaign_id',
        'urls_clicked',
    )
