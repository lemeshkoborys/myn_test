from django.contrib import admin
from django.urls import path, include
from rest_framework_simplejwt.views import (
    TokenObtainPairView,
    TokenRefreshView
)

urlpatterns = [
    path('admin/', admin.site.urls),

    # obtain and refresh jwt
    path('api/v1/token/obtain/', TokenObtainPairView.as_view(), name='token_obtain'),
    path('api/v1/token/refresh/', TokenRefreshView.as_view(), name='token_refresh'),

    # accounts urls
    path('api/v1/accounts/', include('myn_test.accounts.api.urls')),

    # campaings urls
    path('api/v1/performance-chat/',
         include('myn_test.performance_chart.api.urls')),
    path('api/v1/progress-chat/',
         include('myn_test.progress_chart.api.urls')),
]
