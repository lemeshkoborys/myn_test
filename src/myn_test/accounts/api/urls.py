from django.urls import path
from . import views

urlpatterns = [
    path('sign-up/', views.UserSignUpViewSet.as_view({'post': 'create'}), name='sign_up'),
    path('update/', views.UserUpdateViewSet.as_view({'put': 'update'}), name='account_update'),
    path('current-user/', views.CurrentUserAPI.as_view({'get': 'retrieve'}), name='current_user'),
]
