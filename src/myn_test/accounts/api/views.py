from rest_framework.mixins import (
    CreateModelMixin,
    UpdateModelMixin,
    RetrieveModelMixin
)
from rest_framework.viewsets import GenericViewSet
from rest_framework import permissions
from myn_test.accounts.models import User
from . import serializers


class UserSignUpViewSet(CreateModelMixin, GenericViewSet):

    queryset = User.objects.all()
    serializer_class = serializers.UserBaseModelSerializer
    permission_classes = (permissions.AllowAny, )


class UserUpdateViewSet(UpdateModelMixin, GenericViewSet):

    serializer_class = serializers.UserBaseModelSerializer

    def get_object(self):
        return self.request.user


class CurrentUserAPI(RetrieveModelMixin, GenericViewSet):
    serializer_class = serializers.CurrentUserModelSerializer

    def get_object(self):
        return self.request.user
