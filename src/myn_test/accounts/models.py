from django.db import models
from django.contrib.auth.models import AbstractUser, BaseUserManager

from encrypted_model_fields.fields import EncryptedCharField


class UserManager(BaseUserManager):

    def create_user(self, email, password, **extra_fields):
        if not email:
            raise ValueError('The given email must be set')
        email = self.normalize_email(email)
        user = self.model(email=email, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, password, **extra_fields):
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)
        return self.create_user(email, password, **extra_fields)


class User(AbstractUser):

    username = None
    email = models.EmailField(unique=True)
    mailchimp_api_key = EncryptedCharField(
        max_length=256,
        null=True,
        blank=True,
        unique=True,
        default=None
    )
    mailchimp_username = models.CharField(
        max_length=120,
        null=True,
        blank=True,
        unique=True,
        default=None
    )
    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []
    objects = UserManager()
