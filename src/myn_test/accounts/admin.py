from django.contrib import admin
from . import models


@admin.register(models.User)
class UserModelAdmin(admin.ModelAdmin):

    list_display = (
        'id',
        'email',
        'first_name',
        'last_name',
        'mailchimp_api_key',
    )
